import json

from dash import html, dcc, callback, Input, Output, State, ALL, no_update

from dash_iconify import DashIconify
import dash_bootstrap_components as dbc

import dash
from dash import html, dcc, callback, Input, Output, State, no_update
import dash_mantine_components as dmc
import requests

all_icons = [
    'game-icons:globe',
    'mdi:events',
    'simple-line-icons:rocket',
]


# Меню с правильным позиционированием с помощью CSS
def navbar():
    menu = requests.get('http://185.201.28.47:6677/menu').json()
    dropdown_items = [
        dbc.DropdownMenuItem(
            page['title'],
            id={'type': 'dynamic-link', 'index': idx},
            n_clicks=0,
            key=idx  # Adding a unique key for React components
        ) for idx, page in enumerate(menu)
    ]

    return html.Div(
        [
            dbc.DropdownMenu(
                children=dropdown_items,
                label=html.Span(DashIconify(icon='zondicons:menu', width=20),
                                style={'background': 'none', 'display': 'inline-block'}),

                right=True,
                nav=False,
                in_navbar=False,
                toggle_style={
                    "textTransform": "uppercase",
                    "background": "none",
                    "background-image": "none",  # Стиль, убирающий стрелку
                    "border": "none"
                },
                toggleClassName="fst-italic border border-dark",
            )
        ],
        style={'width': '100%', 'display': 'flex', 'justifyContent': 'flex-end'}
    )


@callback(
    [Output({'type': 'menu-text', 'index': ALL}, 'children')],
    [Input({'type': 'dynamic-link', 'index': ALL}, 'n_clicks')],
    prevent_initial_call=True
)
def update_menu_text(n_clicks):
    ctx = dash.callback_context
    if not ctx.triggered:
        return dash.no_update
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]
        button_index = json.loads(button_id)['index']

        # Возвращаем новые тексты для всех элементов, изменяем только нажатый
        return ['New Text if Clicked' if i == button_index else dash.no_update for i in range(len(n_clicks))]


@callback(
    Output('main-text', 'children'),
    [Input({'type': 'dynamic-link', 'index': ALL}, 'n_clicks')],
    prevent_initial_call=True
)
def update_text(n_clicks):
    ctx = dash.callback_context
    if not ctx.triggered:
        return no_update  # Возвращает текущий текст, если коллбек не был вызван событием
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]
        idx = int(json.loads(button_id)['index'])
        menu = requests.get('http://185.201.28.47:6677/menu').json()  # Загружаем меню снова или можно кэшировать его
        return menu[idx]['text']  # Возвращаем текст, соответствующий выбранному пункту меню


@callback(
    Output('full-modal', 'opened'),
    Input('nav-btn', 'n_clicks'),
    State('full-modal', 'opened'),
    prevent_initial_call=True
)
def toggle_modal(_, opened):
    return not opened


@callback(
    Output('full-modal', 'opened', allow_duplicate=True),
    Input({'type': 'dynamic-link', 'index': ALL}, 'n_clicks'),
    State('full-modal', 'opened'),
    prevent_initial_call=True
)
def update_modal(n, opened):
    if True in n:
        return not opened
    return opened
